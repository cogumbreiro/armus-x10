class ClockFinish4 {
    public static def main(Rail[String]) {
        val c = Clock.make();
        // a *very* deep deadlock
        finish async clocked(c)
        finish async clocked(c)
        finish async clocked(c)
        finish async clocked(c)
        finish async clocked(c) {
			c.advance();
        }
    }
}
