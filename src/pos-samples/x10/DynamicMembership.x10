public class DynamicMembership {
    public static def main(Rail[String]) {
		val a = Clock.make();
		val b = Clock.make();
		async clocked(a,b) {
			a.advance();
		}
		b.advance();
	}
}
