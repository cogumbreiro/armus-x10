public class SplitPhase {
	static val skipDeadlock:Boolean = false; // deadlock will happen

    public static def main(Rail[String]) {
		val a = Clock.make();
		val b = Clock.make();
		async clocked(a, b) {
			if (skipDeadlock) {
				b.resume();
			}
			a.advance();
		}
		b.advance();
	}
}
