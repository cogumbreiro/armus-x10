public class ProducerConsumer3 {
	static val nz:Long = 1000;

    public static def main(Rail[String]) {
		val num_threads = 3;
		val ph = new Rail[Clock](num_threads, (x:Long) => Clock.make());
		for (i in 0..(num_threads-1)) {
			val curr = ph(i);
			val neighbour = ph((i - 1 + num_threads) % num_threads);
			val id = i;
			async clocked(curr, neighbour) {
				for (k in 1..(nz - 2)) {
					step(k);
					/* we introduce the deadlock by commenting one of the 
					   boundary conditions */
					//if (id > 0) {
						neighbour.advance();
					//}
					step2(k);
					//if (id < num_threads - 1) {
						curr.advance();
					//}
				}
			}
		}
	}
	
	static def step2(k:Long) {
		// do nothing
	}

	static def step(k:Long) {
		// do nothing
	}
}
