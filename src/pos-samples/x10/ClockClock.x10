class ClockClock {
    public static def main(Rail[String]) {
        val c1 = Clock.make();
        val c2 = Clock.make();
        async clocked(c1, c2) {
            c1.advance();
        }
        async clocked(c1, c2) {
            c2.advance();
        }
    }
}
