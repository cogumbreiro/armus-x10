class DistClockClock {
    public static def main(Rail[String]) {
        val c1 = Clock.make();
        val c2 = Clock.make();
        at(Place.FIRST_PLACE) async clocked(c1, c2) {
            c1.advance();
        }
        at(Place.FIRST_PLACE.next()) async clocked(c1, c2) {
            c2.advance();
        }
    }
}
