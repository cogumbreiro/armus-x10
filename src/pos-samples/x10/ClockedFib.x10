public class ClockedFib {
    public static def main(s:Rail[String]) {
        val N:Int = s.size > 0 ? Int.parseInt(s(0)) : 1000n;
        Console.OUT.println("Started!");
        finish async {
            val x = new Clocked[Int](1n);
            val y = new Clocked[Int](1n);
            async clocked(x.clock, y.clock) {
                while (y() < N) {
                    x() = x() + y();
                    x.next();
                    y.next();
                }
                x() = (Int.MAX_VALUE);
            }
            async clocked(x.clock, y.clock) {
                while (x() < N) {
                    y() = x();
                    Console.OUT.print(" " +  x());
                    y.next();
                    x.next();
                    
                }
                y() = (Int.MAX_VALUE);
                Console.OUT.println();
            }
        }
    }

    public static class Clocked[T](clock:Clock) implements ()=>T{
        var a:Rail[T];
        val name:String; // for documentation

        /** Construct a Clocked[T] with initial value x,and name "".
            Construct a new clock for the returned object.
         */
        public def this(x:T) {
            this(x,Clock.make(), "");
        }

        /** Construct a Clocked[T] with initial value x, clock c, and name "".
         */
        public def this(x:T, c:Clock) {
          this(x, c, "");
        }

        /** Construct a Clocked[T] with initial value x, clock c, and name s.
         */
        public def this(x:T, c:Clock, s:String) {
            property(c);
            this.a= new Rail[T](2, x); 
            this.name=s;
        }
        /**
           Throw an exception unless the current activity is clcoekd on
           this.clock.  Move to the next phase of the clock shifting the
           value from a(1) to a(0) before doing so.
         */
        public def next() {
            clock.advance(); // This is necessary to avoid a read/write race on a(0).
            a(0)=a(1);
            clock.advance(); // This is necessary to avoid a read/write race on a(1).
        }

        /**
           Return the current value of the Clocked object
        */
        public operator this() = a(0);

        /** Assign the next value.
         */
        public operator this() = (x:T) { a(1)=x; }
        public def toString() =name;
    }
}
