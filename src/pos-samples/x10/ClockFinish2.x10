class ClockFinish2 {
    public static def main(Rail[String]) {
        val c = Clock.make();
        finish {
            async clocked(c) {
                c.advance();
            }
        }
    }
}
