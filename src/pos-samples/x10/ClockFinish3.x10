class ClockFinish3 {
    public static def main(Rail[String]) {
        val c = Clock.make();
        // a very deep deadlock
        finish finish finish finish finish {
            async clocked(c) {
                c.advance();
            }
        }
    }
}
