/**
 * Iterative averaging example.
 * @author Tiago Cogumbreiro
 *
 */
public class IterativeAveraging1D_v2 {
	static val I:Long = 20;
	static val J:Long = 100;
	static val a = new Rail[Long](I + 2);
    public static def main(Rail[String]) {
		val c = Clock.make(); // Cyclic barrier
		finish {
			for (i in 1..I) {
				async clocked(c) { // Spawn task i
					for (var j:Long = 1; j <= J; j++) {
						val l = a(i - 1);
						val r = a(i + 1);
						Clock.advanceAll();
						a(i) = (l + r) / 2;
						Clock.advanceAll();
					}
				}
			}
		}
		handle(a);
	}
	private static def handle(array:Rail[Long]) {
		Console.OUT.println(array);
	}
}
