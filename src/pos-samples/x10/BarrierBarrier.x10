class BarrierBarrier {
    public static def main(Rail[String]) {
        val c1 = new x10.util.concurrent.SPMDBarrier(2N);
        val c2 = new x10.util.concurrent.SPMDBarrier(2N);
        async {
        	c1.register(); c2.register();
            c1.advance();
        }
        async {
        	c1.register(); c2.register();
            c2.advance();
        }
    }
}
