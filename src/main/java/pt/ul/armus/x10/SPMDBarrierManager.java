package pt.ul.armus.x10;

import java.util.Arrays;
import java.util.Collection;

import pt.ul.armus.Resource;
import x10.util.concurrent.SPMDBarrier;

class SPMDBarrierManager implements ResourceManager<SPMDBarrier> {
	final private PhaseManager<SPMDBarrier> phases = new PhaseManager<>();
	
	public void register(SPMDBarrier barrier) {
		phases.register(barrier, 0);
	}
	
	@Override
	public Collection<Resource> getAllocated() {
		return Arrays.asList(phases.getRegistered());
	}

	public Resource requestOne(SPMDBarrier blocked) {
		ResourceVariable resource = phases.getResource(blocked);
		return resource;
	}

	public void advance(SPMDBarrier barrier) {
		phases.advance(barrier);
	}
	
	@Override
	public void clear() {
		phases.clear();
	}
	
}
