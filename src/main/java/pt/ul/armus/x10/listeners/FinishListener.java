package pt.ul.armus.x10.listeners;

import x10.lang.FinishState;

/**
 * Observes {@link FinishState}.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public interface FinishListener {
	/**
	 * The activity reached the end of a finish block.
	 * @param finish
	 */
	void beforeFinish(FinishState finish);
	/**
	 * The activity passed the end of a finish block.
	 * @param finish
	 */
	void afterFinish(FinishState finish);
	/**
	 * The activity entered a finish scope.
	 * @param finish
	 */
	void onRegister(FinishState finish);
}
