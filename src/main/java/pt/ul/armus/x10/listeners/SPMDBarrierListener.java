package pt.ul.armus.x10.listeners;

import x10.util.concurrent.SPMDBarrier;

/**
 * Observes {@link SPMDBarrier}s.
 * @author tsoaresc
 *
 */
public interface SPMDBarrierListener {
	/**
	 * An activity invoked {@link SPMDBarrier#register()}.
	 * @param barrier
	 */
	void onRegister(SPMDBarrier barrier);
	/**
	 * An activity invoked {@link SPMDBarrier#advance()}.
	 * @param barrier
	 */
	void beforeAdvance(SPMDBarrier barrier);
	/**
	 * After invoking {@link SPMDBarrier#advance()}.
	 * @param barrier
	 */
	void afterAdvance(SPMDBarrier barrier);
}
