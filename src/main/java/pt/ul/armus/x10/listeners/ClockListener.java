package pt.ul.armus.x10.listeners;

import x10.lang.Clock;

/**
 * Observes {@link Clock}s.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public interface ClockListener {
	/**
	 * The current activity invokes {@link Clock#advance()}. 
	 * @param clock
	 */
	void beforeAdvance(Clock clock);
	/**
	 * After the execution of {@link Clock#advance()}. 
	 * @param clock
	 */
	void afterAdvance(Clock clock);
	/**
	 * The current activity invokes {@link Clock#advanceAll()}. 
	 */
	void beforeAdvanceAll();
	/**
	 * After the execution of {@link Clock#advanceAll()}. 
	 */
	void afterAdvanceAll();
}
