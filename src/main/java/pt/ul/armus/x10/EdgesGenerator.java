package pt.ul.armus.x10;

import java.util.Collection;
import java.util.logging.Logger;

import pt.ul.armus.Resource;
import pt.ul.armus.deadlockresolver.DeadlockFoundException;
import pt.ul.armus.edgebuffer.EdgeSetFactory;
import pt.ul.armus.edgebuffer.TaskHandle;
import x10.lang.Activity;
import x10.lang.Clock;
import x10.lang.ClockUseException;
import x10.lang.FinishState;
import x10.util.concurrent.SPMDBarrier;

class EdgesGenerator {
	private static final Logger LOG = Logger.getLogger(EdgesGenerator.class.getName());
	private final Activity act;
	private final TaskHandle handle;
	private final FinishManager finishes = new FinishManager();
	private final ClockManager clocks;
	private final SPMDBarrierManager barriers = new SPMDBarrierManager();

	public EdgesGenerator(Activity act, TaskHandle handle, Iterable<FinishState> registeredFinishes) {
		this.act = act;
		this.handle = handle;
		clocks = new ClockManager(act);
		initFinish(registeredFinishes);
	}

	// FinishState

	private void initFinish(Iterable<FinishState> registeredFinishes) {
		for (FinishState fin : registeredFinishes) {
			registerFinish(fin);
		}
	}
	
	public void registerFinish(FinishState finish) {
		finishes.register(finish);
	}

	public void blockFinish() throws DeadlockFoundException {
		Resource req = finishes.requestOne(act.finishState);
		Collection<Resource> alloced = getAllocated();
		handle.setBlocked(EdgeSetFactory.requestOneAllocateMany(req, alloced));
	}

	private Collection<Resource> getAllocated() {
		Collection<Resource> alloced = clocks.getAllocated();
		alloced.addAll(finishes.getAllocated());
		alloced.addAll(barriers.getAllocated());
		return alloced;
	}

	// SPMDBarrier

	public void registerSPMDBarrier(SPMDBarrier barrier) {
		barriers.register(barrier);
	}

	public void blockSPMDBarrierAdvance(SPMDBarrier barrier) throws ClockUseException {
		Resource req = barriers.requestOne(barrier);
		barriers.advance(barrier);
		Collection<Resource> alloced = getAllocated();
		try {
			handle.setBlocked(EdgeSetFactory.requestOneAllocateMany(req, alloced));
		} catch (DeadlockFoundException e) {
			LOG.warning("Deadlock avoided " + e);
			throw toClockUseException(e);
		}
	}

	// Clock

	public void blockClockAdvance(Clock clock) throws ClockUseException {
		try {
			Resource req = clocks.requestOne(clock);
			Collection<Resource> alloced = clocks.advanceAndGetAllocated(clock);
			alloced.addAll(finishes.getAllocated());
			alloced.addAll(barriers.getAllocated());
			try {
				handle.setBlocked(EdgeSetFactory.requestOneAllocateMany(req, alloced));
			} catch (DeadlockFoundException e) {
				LOG.warning("Deadlock avoided " + e);
				throw toClockUseException(e);
			}
		} catch (UnregisteredActivityException e) {
			// This exception is raised when the user is trying to manipulate a
			// clock it is not registered with. Do nothing and it will crash.
		}
	}

	public void blockClockAdvanceAll() throws ClockUseException {
		Collection<Resource> reqs = clocks.requestAll();
		Collection<Resource> alloced = finishes.getAllocated();
		alloced.addAll(barriers.getAllocated());
		try {
			handle.setBlocked(EdgeSetFactory.requestManyAllocateMany(reqs, alloced));
		} catch (DeadlockFoundException e) {
			LOG.warning("Deadlock avoided " + e);
			throw toClockUseException(e);
		}
	}

	// Others

	public void destroy() {
		clocks.clear();
		finishes.clear();
		barriers.clear();
	}

	public static ClockUseException toClockUseException(DeadlockFoundException e) {
		return new ClockUseException("Deadlock found: " + e.getDeadlock());
	}

	public void clearBlocked() {
		if (handle.isBlocked()) {
			handle.clearBlocked();
		}
	}
}
