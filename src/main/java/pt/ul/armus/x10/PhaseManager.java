package pt.ul.armus.x10;

import pt.ul.armus.Resource;

import com.carrotsearch.hppc.ObjectIntOpenHashMap;

/**
 * Maintains the registered resources.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@di.fc.ul.pt)
 * 
 */
public class PhaseManager<T> {
	/*
	 * Obtains the phase of each registered synchronization object. The range is
	 * the set of registered synchronization objects.
	 */
	private ObjectIntOpenHashMap<T> phases = new ObjectIntOpenHashMap<>();

	/**
	 * Checks if the synchronization object is being managed.
	 * 
	 * @param synch
	 * @return
	 */
	public boolean isRegistered(T synch) {
		return phases.containsKey(synch);
	}

	/**
	 * Track the synchronization object, starting at a given phase.
	 * 
	 * @param synch
	 * @param phase
	 */
	public void register(T synch, int phase) {
		phases.put(synch, phase);
	}

	/**
	 * De-registers from a synchronization object.
	 * 
	 * @param synch
	 */
	public boolean deregister(T synch) {
		ensureRegistered(synch);
		return phases.remove(synch) > 0;
	}

	/**
	 * Advances the current phase of the object.
	 * 
	 * @param synch
	 *            The synchronization object.
	 * @return The value of the previous phase.
	 */
	public int advance(T synch) {
		ensureRegistered(synch);
		return phases.addTo(synch, 1) - 1;
	}

	/**
	 * @param synch
	 * @throw Throws an exception when the given task is not registered with
	 *        <code>synch</code>.
	 */
	public void ensureRegistered(T synch) throws IllegalStateException {
		if (!phases.containsKey(synch))
			throw new IllegalStateException(
					"Synchronization object is not registered: " + synch);
	}

	/**
	 * Advances the phase of all registered objects
	 */
	public void advanceAll() {
		final int[] values = phases.values;
		final boolean[] allocated = phases.allocated;
		for (int i = 0; i < allocated.length; i++) {
			if (allocated[i]) {
				values[i]++;
			}
		}
	}
	
	/**
	 * Get the phases on which the task is registered with.
	 * 
	 * @return
	 */
	public Resource[] getRegistered() {
		final Object[] keys = phases.keys;
		final int[] values = phases.values;
		final boolean[] allocated = phases.allocated;
		ResourceVariable[] result = new ResourceVariable[phases.size()];
		int index = 0;
		for (int i = 0; i < allocated.length; i++) {
			if (allocated[i]) {
				result[index] = new ResourceVariable(keys[i], values[i]);
				index++;
			}
		}
		return result;
	}

	/**
	 * De-registers from every resource.
	 */
	public void clear() {
		phases.clear();
	}

	/**
	 * Return the phase number of the given synchronisation object.
	 * @param synch
	 * @return
	 */
	public int getPhase(T synch) {
		ensureRegistered(synch);
		return phases.get(synch);
	}
	
	/**
	 * Returns the resource associated with the current object
	 * @param synch
	 * @return
	 */
	public ResourceVariable getResource(T synch) {
		return new ResourceVariable(synch, getPhase(synch));
	}
}