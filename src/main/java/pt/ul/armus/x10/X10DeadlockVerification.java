package pt.ul.armus.x10;

import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Logger;

import pt.ul.armus.Armus;
import pt.ul.armus.deadlockresolver.DeadlockFoundException;
import pt.ul.armus.x10.listeners.ClockListener;
import pt.ul.armus.x10.listeners.FinishListener;
import pt.ul.armus.x10.listeners.SPMDBarrierListener;
import x10.lang.Activity;
import x10.lang.Clock;
import x10.lang.ClockUseException;
import x10.lang.FinishState;
import x10.lang.Runtime;
import x10.util.concurrent.SPMDBarrier;
import x10.util.concurrent.SimpleLatch;
import static pt.ul.armus.x10.EdgesGenerator.toClockUseException;

//XXX: At some point migrate to: https://docs.guava-libraries.googlecode.com/git/javadoc/com/google/common/cache/CacheBuilder.html#weakKeys()
/**
 * A global facade for the listeners of each class being verified.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
class X10DeadlockVerification implements ClockListener, FinishListener,
		SPMDBarrierListener {
	private static final Logger LOG = Logger.getLogger(X10DeadlockResolver.class.getName());
	private final Armus verify;
	private final Map<Activity, EdgesGenerator> handles = new WeakHashMap<>();
	private final FinishTracker tracker = new FinishTracker();

	public X10DeadlockVerification(Armus verify) {
		this.verify = verify;
	}

	private EdgesGenerator get() {
		return get(Runtime.activity());
	}

	private EdgesGenerator get(Activity act) {
		EdgesGenerator edge = handles.get(act);
		if (edge == null) {
			edge = new EdgesGenerator(act, verify.getBuffer().createHandle(),
					tracker.getFinishTree(act.finishState));
			handles.put(act, edge);
		}
		return edge;
	}

	@Override
	public void beforeAdvance(Clock clock) throws ClockUseException {
		get().blockClockAdvance(clock);
	}

	@Override
	public void afterAdvance(Clock clock) throws ClockUseException {
		get().clearBlocked();
	}

	@Override
	public void onRegister(FinishState previous) {
		FinishState current = Runtime.activity().finishState;
		tracker.linkToParent(current, previous);
		EdgesGenerator gen = get();
		gen.registerFinish(previous);
		gen.registerFinish(current);
	}

	@Override
	public void beforeFinish(FinishState fin) {
		try {
			get().blockFinish();
		} catch (DeadlockFoundException | RuntimeException e) {
			LOG.warning("Deadlock avoided " + e);
			// we have to catch runtime exceptions, because otherwise, any bug
			// we introduce is silently eaten by the activity, this way
			// the error propagates
			RuntimeException runtimeEx = e instanceof DeadlockFoundException ?
					toClockUseException((DeadlockFoundException) e)
					: (RuntimeException) e;
			FinishState finishState = Runtime.activity().finishState();
			finishState.pushException(runtimeEx);
			// break the deadlock
			finishState.notifyActivityTermination();
			SimpleLatch simpleLatch = finishState.simpleLatch();
			if (simpleLatch != null) {
				simpleLatch.release();
			}
		}
	}

	@Override
	public void afterFinish(FinishState fin) throws ClockUseException {
		get().clearBlocked();
	}

	@Override
	public void beforeAdvanceAll() throws ClockUseException {
		get().blockClockAdvanceAll();
	}

	@Override
	public void afterAdvanceAll() throws ClockUseException {
		get().clearBlocked();
	}

	public void start() {
		verify.start();
	}

	public void stop() {
		verify.stop();
	}

	@Override
	public void onRegister(SPMDBarrier barrier) {
		get().registerSPMDBarrier(barrier);
	}

	@Override
	public void beforeAdvance(SPMDBarrier barrier) {
		get().blockSPMDBarrierAdvance(barrier);
	}

	@Override
	public void afterAdvance(SPMDBarrier barrier) {
		get().clearBlocked();
	}

}
