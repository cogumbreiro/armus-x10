package pt.ul.armus.x10;

import pt.ul.armus.Resource;
import x10.lang.Clock;

public class ClockVariable implements Resource {

	private final Clock clock;
	private final int phase;
	
	public ClockVariable(Clock clock, int phase) {
		this.clock = clock;
		this.phase = phase;
	}

	@Override
	public Clock getSynch() {
		return clock;
	}

	public int getPhase() {
		return phase;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + phase;
		result = prime * result + ((clock == null) ? 0 : clock.root.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClockVariable other = (ClockVariable) obj;
		if (phase != other.phase)
			return false;
		return clock.root.hashCode() == other.clock.root.hashCode();
	}

	@Override
	public String toString() {
		return "ClockResource(id=" + clock.root.hashCode() + ", phase=" + phase + ")";
	}
}
