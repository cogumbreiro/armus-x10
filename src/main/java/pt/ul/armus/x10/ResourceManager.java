package pt.ul.armus.x10;

import java.util.Collection;

import pt.ul.armus.Resource;

interface ResourceManager<T> {
	Collection<Resource> getAllocated();
	void clear();
}
