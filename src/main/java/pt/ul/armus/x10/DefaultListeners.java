package pt.ul.armus.x10;

import static pt.ul.armus.conf.ConfigurationLoader.getDefault;

import java.util.logging.Logger;

import pt.ul.armus.Armus;
import pt.ul.armus.ArmusFactory;
import pt.ul.armus.conf.ConfigurationLoader;
import pt.ul.armus.conf.MainConfiguration;
import pt.ul.armus.conf.in.ParserException;
import pt.ul.armus.x10.listeners.ClockListener;
import pt.ul.armus.x10.listeners.FinishListener;
import pt.ul.armus.x10.listeners.SPMDBarrierListener;
/**
 * The default listeners are accessed by the instrumentation layer. Each
 * listener communicates dependency edges with Armus when we have a blocking
 * call.
 * 
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 * 
 */
public class DefaultListeners {
	public static final String CONFIGURATION_PREFIX = "armus";
	static {
		Logger log = Logger.getLogger(DefaultListeners.class.getName());
		X10DeadlockVerification value = null;
		Armus armus = null;
		try {
			// we override the default configuration, since X10 has its own
			// machinery to handle I/O and the application.
			MainConfiguration defs = getDefault();
			defs.deadlockResolver = X10DeadlockResolver.DEFAULT;
			// by default we do not do avoidance, as it is unreliable
			defs.avoidanceEnabled = false;
			defs.detection.isEnabled = true;
			// load the configuration, overriding defaults
			MainConfiguration conf = ConfigurationLoader.parseFromSystemWithDefaults(CONFIGURATION_PREFIX, defs);
			armus = ArmusFactory.build(conf);
			value = new X10DeadlockVerification(armus);
			value.start();
		} catch (ParserException e) {
			value = null;
			log.severe("Could not initialize Armus-X10: " + e);
			throw new RuntimeException(e);
		}
		ARMUS = armus;
		INSTANCE = value;
	}
	private static final X10DeadlockVerification INSTANCE;
	protected static final Armus ARMUS;

	/**
	 * Returns the default finish listener.
	 * 
	 * @return
	 */
	public static FinishListener getFinishListener() {
		return INSTANCE;
	}

	/**
	 * Returns the default clock listener.
	 * 
	 * @return
	 */
	public static ClockListener getClockListener() {
		return INSTANCE;
	}

	/**
	 * Returns the default barrier listener.
	 * 
	 * @return
	 */
	public static SPMDBarrierListener getSPMDBarrierListener() {
		return INSTANCE;
	}
}
