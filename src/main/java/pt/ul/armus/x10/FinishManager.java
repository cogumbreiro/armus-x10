package pt.ul.armus.x10;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import pt.ul.armus.Resource;
import x10.lang.Activity;
import x10.lang.FinishState;
import x10.lang.Runtime;

class FinishManager implements ResourceManager<FinishState> {
	private final Set<FinishState> finishes = new HashSet<>();

	public void register(FinishState finish) {
		finishes.add(finish);
	}

	private static Collection<Resource> asResources(
			Collection<FinishState> finishes) {
		ArrayList<Resource> result = new ArrayList<>(finishes.size());
		for (FinishState finish : finishes) {
			result.add(new FinishVariable(finish));
		}
		return result;
	}

	/**
	 * Returns a read-only view of the registered finishes.
	 * 
	 * @return
	 */
	public Collection<FinishState> getFinishes() {
		return Collections.unmodifiableCollection(finishes);
	}

	@Override
	public Collection<Resource> getAllocated() {
		return asResources(finishes);
	}

	public Resource requestOne(FinishState blocked) {
		if (!finishes.remove(blocked)) {
			Activity activity = Runtime.activity();
			String msg = activity.toString();
			msg += " waiting on an unregistered finish: " + blocked;
			msg += " not in " + finishes; 
			msg += " activity's finish is " + activity.finishState;
			throw new IllegalStateException(msg);
		}
		return new FinishVariable(blocked);
	}

	@Override
	public void clear() {
		finishes.clear();
	}
}
