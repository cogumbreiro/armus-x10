package pt.ul.armus.x10;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.WeakHashMap;

import x10.lang.FinishState;

public class FinishTracker {
	static class FinishLink {
		final FinishState curr;
		final FinishLink parent;

		public FinishLink(FinishState curr, FinishLink parent) {
			this.curr = curr;
			this.parent = parent;
		}
	}

	static class FinishLinkIterator implements Iterator<FinishState> {
		private FinishLink link;

		public FinishLinkIterator(FinishLink link) {
			this.link = link;
		}

		@Override
		public boolean hasNext() {
			return link != null;
		}

		@Override
		public FinishState next() {
			if (link == null) {
				throw new NoSuchElementException();				
			}
			FinishState state = link.curr;
			link = link.parent;
			return state;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	static class FinishLinkIterable implements Iterable<FinishState> {
		private final FinishLink link;

		public FinishLinkIterable(FinishLink link) {
			this.link = link;
		}

		@Override
		public Iterator<FinishState> iterator() {
			return new FinishLinkIterator(link);
		}
	}

	private WeakHashMap<FinishState, FinishLink> links = new WeakHashMap<>();

	/**
	 * Links the given finish to its parent
	 * @param finish
	 * @param parent
	 */
	public void linkToParent(FinishState finish, FinishState parent) {
		FinishLink link = links.get(parent);
		if (link == null) {
			link = new FinishLink(parent, null);
			links.put(parent, link);
		}
		links.put(finish, new FinishLink(finish, link));
	}

	public Iterable<FinishState> getFinishTree(FinishState finish) {
		if (finish == null) {
			throw new IllegalArgumentException("Finish cannot be null");
		}
		FinishLink link = links.get(finish);
		link = link == null ? new FinishLink(finish, null) : link;
		return new FinishLinkIterable(link);
	}
}
