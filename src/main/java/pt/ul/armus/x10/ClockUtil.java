package pt.ul.armus.x10;

import java.util.ArrayList;
import java.util.Collection;
import java.util.NoSuchElementException;

import x10.lang.Activity;
import x10.lang.Clock;
import x10.lang.Iterator;
import x10.util.Set;

public class ClockUtil {
	/**
	 * Obtains the registered clocks of a given activity.
	 * @param activity
	 * @return
	 */
	public static Collection<Clock> getRegisteredClocks(Activity activity) throws NoSuchElementException {
		@SuppressWarnings("rawtypes")
		Set clocks = activity.clockPhases().keySet();
		ArrayList<Clock> result = new ArrayList<>((int) clocks.size$O());
		@SuppressWarnings("rawtypes")
		Iterator iter = clocks.iterator();
		while (iter.hasNext$O()) {
			result.add((Clock) iter.next$G());
		}
		return result;
	}
	
	/**
	 * Checks if resume was invoked.
	 * @param clock
	 * @return
	 * @throws UnregisteredActivityException 
	 */
	public static boolean hasResumed(Activity act, Clock clock) throws UnregisteredActivityException {
		ensureNotNull(act, clock);
		return getPhase(act, clock) < 0;
	}
	
	/**
	 * The signal phase takes into account the resume flag.
	 * @param clock
	 * @return
	 * @throws UnregisteredActivityException 
	 */
	public static int getSignalPhase(Activity act, Clock clock) throws UnregisteredActivityException {
		ensureNotNull(act, clock);
		int phase = getPhase(act, clock);
		return phase < 0 ? Math.abs(phase) + 1 : phase;
	}
	/**
	 * Returns the clock phase.
	 * @param clock
	 * @return
	 * @throws UnregisteredActivityException 
	 */
	public static int getWaitPhase(Activity act, Clock clock) throws UnregisteredActivityException {
		ensureNotNull(act, clock);
		return Math.abs(getPhase(act, clock));
	}

	/**
	 * Throws an {@link IllegalArgumentException} when either is null.
	 * @param act
	 * @param clock
	 */
	private static void ensureNotNull(Activity act, Clock clock) {
		if (act == null || clock == null) {
			throw new IllegalArgumentException("Activity=%s and clock=%s must be non-null.");
		}
	}

	/**
	 * Returns the phase in which the activity is registered with.
	 * @param act
	 * @param clock
	 * @return
	 * @throws UnregisteredActivityException 
	 */
	private static int getPhase(Activity act, Clock clock) throws UnregisteredActivityException {
		try {
			return act.clockPhases().getOrThrow$O(clock);
		} catch (NoSuchElementException e) {
			throw new UnregisteredActivityException(e);
		}
	}

}
