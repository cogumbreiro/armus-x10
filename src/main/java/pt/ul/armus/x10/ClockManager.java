package pt.ul.armus.x10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import pt.ul.armus.Resource;
import x10.lang.Activity;
import x10.lang.Clock;

class ClockManager implements ResourceManager<Clock> {
	
	private final Activity act;

	public ClockManager(Activity act) {
		if (act == null) {
			throw new IllegalArgumentException("The activity cannot be null.");
		}
		this.act = act;
	}

	private Resource advanceResource(Clock clock) throws UnregisteredActivityException {
		return new ClockVariable(clock, ClockUtil.getWaitPhase(act, clock) + 1);
	}

	private Resource createWaitResource(Clock clock) throws UnregisteredActivityException {
		return new ClockVariable(clock, ClockUtil.getWaitPhase(act, clock));
	}
	
	private Resource createSignalResource(Clock clock) throws UnregisteredActivityException {
		return new ClockVariable(clock, ClockUtil.getSignalPhase(act, clock));
	}
	
	@Override
	public Collection<Resource> getAllocated() {
		Collection<Resource> regs = new ArrayList<>();
		try {
			for (Clock reg : ClockUtil.getRegisteredClocks(act)) {
				regs.add(createSignalResource(reg));
			}
		} catch (UnregisteredActivityException e) {
			throw new IllegalStateException("Should never happen.", e);
		}
		return regs;
	}

	public Collection<Resource> advanceAndGetAllocated(Clock toAdvance) throws UnregisteredActivityException {
		Collection<Resource> regs = new ArrayList<>();
		for (Clock reg : ClockUtil.getRegisteredClocks(act)) {
			if (reg == toAdvance) {
				regs.add(advanceResource(reg));
			} else {
				regs.add(createSignalResource(reg));
			}
		}
		return regs;
	}

	public Resource requestOne(Clock clock) throws UnregisteredActivityException {
		return new ClockVariable(clock, ClockUtil.getWaitPhase(act, clock));
	}
	
	public Collection<Resource> requestAll() {
		Clock[] blocked = getClocks();
		Resource[] result = new Resource[blocked.length];
		try {
			for (int i = 0; i < blocked.length; i++) {
					result[i] = createWaitResource(blocked[i]);
			}
		} catch (UnregisteredActivityException e) {
			throw new IllegalStateException("Should never happen.", e);
		}
		return Arrays.asList(result);
	}
	
	private Clock[] getClocks() {
		return ClockUtil.getRegisteredClocks(act).toArray(new Clock[0]);
	}

	@Override
	public void clear() {
		// Nothing to do
	}
}
