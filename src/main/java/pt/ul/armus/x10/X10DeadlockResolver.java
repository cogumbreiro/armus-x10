package pt.ul.armus.x10;

import pt.ul.armus.DeadlockInfo;
import pt.ul.armus.deadlockresolver.DeadlockResolver;
import x10.io.Console;

public class X10DeadlockResolver implements DeadlockResolver {

	@Override
	public void onDeadlockDetected(DeadlockInfo deadlock) {
		Console.get$ERR().println("Deadlock detected: " + deadlock);
		x10.lang.System.setExitCode(255);
		x10.lang.System.killHere();
	}

	public static final DeadlockResolver DEFAULT = new X10DeadlockResolver();
}
