package pt.ul.armus.x10;

import pt.ul.armus.Resource;
import x10.util.concurrent.SPMDBarrier;

public class SPMDBarrierEvent implements Resource {
	private final SPMDBarrier synch;
	private final int phase;
	
	/**
	 * @param phase
	 * @param synch
	 */
	public SPMDBarrierEvent(SPMDBarrier synch, int phase) {
		this.synch = synch;
		this.phase = phase;
	}


	@Override
	public SPMDBarrier getSynch() {
		return synch;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + phase;
		result = prime * result + ((synch == null) ? 0 : synch.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SPMDBarrierEvent other = (SPMDBarrierEvent) obj;
		if (phase != other.phase)
			return false;
		if (synch == null) {
			if (other.synch != null)
				return false;
		} else if (!synch.equals(other.synch))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return phase + ":" + synch;
	}
}
