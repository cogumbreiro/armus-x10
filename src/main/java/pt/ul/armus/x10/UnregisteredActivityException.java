package pt.ul.armus.x10;

/**
 * The activity is not registered with the given clock.
 */
public class UnregisteredActivityException extends Exception {
	private static final long serialVersionUID = -8353045002882118053L;

	public UnregisteredActivityException(Throwable cause) {
		super(cause);
	}
}
