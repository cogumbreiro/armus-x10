package pt.ul.armus.x10;

import pt.ul.armus.Resource;
import x10.lang.FinishState;

public class FinishVariable implements Resource {

	private final FinishState finish;

	public FinishVariable(FinishState finish) {
		this.finish = finish;
	}
	
	@Override
	public FinishState getSynch() {
		return finish;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((finish == null) ? 0 : System.identityHashCode(finish));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinishVariable other = (FinishVariable) obj;
		return finish.equals(other.finish);
	}

	@Override
	public String toString() {
		return finish.toString();
	}
}
