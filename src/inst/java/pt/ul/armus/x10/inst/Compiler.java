package pt.ul.armus.x10.inst;

public class Compiler {
	private static final String ASPECTS_JAR = "aspects.jar";
	private static final String RT_JAR = "rt.jar";
	
	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("Usage: armusc <INTPUT> <OUTPUT.JAR>");
			System.err
					.println("\tINPUT\t\tdirectory or ZIP/JAR archive holding the compiled code");
			System.err.println("\tOUTPUT\t\tthe output location (a JAR)");
			System.exit(-1);
		}
		Instrumenter inst = new Instrumenter(Compiler.class, ASPECTS_JAR, RT_JAR);
		inst.compile(args[0], args[1]);
	}
}
