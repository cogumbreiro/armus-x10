package pt.ul.armus.x10.inst;

import pt.ul.armus.x10.DefaultListeners;
import pt.ul.armus.x10.listeners.ClockListener;
import x10.lang.Clock;

public aspect ClockObserver {
	pointcut advance(Clock clock) : call(public void Clock.advance()) && target(clock);
    pointcut advanceAll() : call(public void Clock.advanceAll());
    
    private ClockListener listener = DefaultListeners.getClockListener();
    
    public void setListener(ClockListener listener) {
		this.listener = listener;
	}
    
	void around(Clock clock): advance(clock) {
		listener.beforeAdvance(clock);
		try {
			proceed(clock);
		} finally {
			listener.afterAdvance(clock);
		}
	}
	
    void around(): advanceAll() {
        listener.beforeAdvanceAll();
		try {
			proceed();
		} finally {
			listener.afterAdvanceAll();
		}
    }
}
