package pt.ul.armus.x10.inst;

import pt.ul.armus.x10.DefaultListeners;
import pt.ul.armus.x10.listeners.SPMDBarrierListener;
import x10.util.concurrent.SPMDBarrier;

public aspect SPMDBarrierObserver {
	pointcut advance(SPMDBarrier barrier) : call(public void SPMDBarrier.advance()) && target(barrier);
	pointcut register(SPMDBarrier barrier) : call(public void SPMDBarrier.register()) && target(barrier);
    
    private SPMDBarrierListener listener = DefaultListeners.getSPMDBarrierListener();
    
    public void setListener(SPMDBarrierListener listener) {
		this.listener = listener;
	}
    
	void around(SPMDBarrier barrier): advance(barrier) {
		listener.beforeAdvance(barrier);
		try {
			proceed(barrier);
		} finally {
			listener.afterAdvance(barrier);
		}
	}
	
    after(SPMDBarrier barrier) returning: register(barrier) {
    	listener.onRegister(barrier);
    }
}
