package pt.ul.armus.x10.inst;

import pt.ul.armus.x10.DefaultListeners;
import pt.ul.armus.x10.listeners.FinishListener;
import x10.lang.FinishState;

public aspect FinishObserver {
	pointcut register() : call(public FinishState x10.lang.Runtime.startFinish(..));
	pointcut stopFinish(FinishState f) : call(public void x10.lang.Runtime.stopFinish(x10.lang.FinishState)) && args(f);
    
	private FinishListener listener = DefaultListeners.getFinishListener();
	
	public void setListener(FinishListener listener) {
		this.listener = listener;
	}
	
    after() returning (FinishState finish): register() {
    	listener.onRegister(finish);
    }
	
	void around(FinishState f) : stopFinish(f) {
		listener.beforeFinish(f);
		try {
			proceed(f);
		} finally {
			listener.afterFinish(f);
		}
	}
}
