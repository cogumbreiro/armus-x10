package pt.ul.armus.x10;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;

import pt.ul.armus.Armus;
import pt.ul.armus.Resource;
import pt.ul.armus.deadlockresolver.DeadlockFoundException;
import pt.ul.armus.edgebuffer.EdgeSetFactory;
import pt.ul.armus.edgebuffer.TaskHandle;
import pt.ul.armus.x10.listeners.ClockListener;
import pt.ul.armus.x10.listeners.FinishListener;
import x10.lang.Activity;
import x10.lang.Clock;
import x10.lang.ClockUseException;
import x10.lang.FinishState;
import x10.lang.Runtime;
import x10.util.concurrent.SimpleLatch;

public class RegressionTests extends InitX10 {

	@Test
	public void clockFinish() {
		ClockListener clockListener = DefaultListeners.getClockListener();
		FinishListener finishListener = DefaultListeners.getFinishListener();
		SimpleLatch l = new SimpleLatch();
		FinishState f = mock(FinishState.class);
		when(f.simpleLatch()).thenReturn(l);
		Activity parent = createActivity();
		Activity child = createActivity(f);
		Clock c = Clock.make();
		// setup activity 1
		setCurrentActivity(parent);
		finishListener.onRegister(f);
		registerRawCurrentActivity(c, 1);
		// setup activity 2
		setCurrentActivity(child);
		//finishListener.onRegister(f);
		registerRawCurrentActivity(c, 1);
		
		//  now setup the deadlock
		
		// activity 1 blocked on clock 'c'
		setCurrentActivity(child);
		clockListener.beforeAdvanceAll();
		// activity 2 blocked on the finish
		setCurrentActivity(parent);
		parent.finishState = f;
		finishListener.beforeFinish(f);
		verify(f).pushException(any(ClockUseException.class));
	}

	@Test
	public void iterativeAveraging_v2() throws DeadlockFoundException {
		FinishState f = Runtime.makeDefaultFinish();
		Clock c = Clock.make();
		Resource r1 = new ClockVariable(c, 1);
		Resource r2 = new FinishVariable(f);
		Resource r3 = new FinishVariable(Runtime.makeDefaultFinish());
		
		Armus armus = DefaultListeners.ARMUS;
		TaskHandle t1 = armus.getBuffer().createHandle();
		t1.setBlocked(EdgeSetFactory.requestOneAllocateOne(r1, r2));
		for (int i = 0; i < 100; i++) {
			TaskHandle t2 = armus.getBuffer().createHandle();
			t2.setBlocked(EdgeSetFactory.requestOneAllocateOne(r2, r3));
		}
		TaskHandle t3 = armus.getBuffer().createHandle();
		try {
			t3.setBlocked(EdgeSetFactory.requestOneAllocateOne(r2, r1));
		} catch (DeadlockFoundException e) {
			// ok
		}
		assertNotNull(armus.checkDeadlock());
	}
}
