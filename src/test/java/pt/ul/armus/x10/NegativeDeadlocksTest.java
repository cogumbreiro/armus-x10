package pt.ul.armus.x10;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import pt.ul.armus.conf.ConfigurationLoader;
import pt.ul.armus.conf.MainConfiguration;
import pt.ul.armus.x10.ExecUtil.ProcessSummary;

/**
 * Samples that are deadlock-free.
 * 
 */
@RunWith(value = Parameterized.class)
public class NegativeDeadlocksTest {
	private static final int TIMEOUT = 10_000; // Milliseconds
	private String className;
	private MainConfiguration opts;

	public NegativeDeadlocksTest(String className, MainConfiguration options) {
		this.className = className;
		this.opts = options;
	}

	@Parameters(name = "{0} {1}")
	public static Collection<Object[]> data() throws IOException {
		List<Object[]> result = new ArrayList<>();
		for (String file : Harness.getNegativeTestNames()) {
			for (MainConfiguration conf : Harness.getConfigurations()) {
				result.add(new Object[] { file, conf });
			}
		}
		return result;
	}

	@Test
	public void test() throws IOException, InterruptedException,
			ExecutionException {
		X10ProcessBuilder x10 = Harness.x10(className);

		Properties props = ConfigurationLoader.render(
				DefaultListeners.CONFIGURATION_PREFIX, opts);
		x10.setSystemProperties(props);
		Map<String, String> env = new HashMap<>();
		// ensure we can test SPMDBarriers
		env.put("X10_NTHREADS",
				Integer.toString(Runtime.getRuntime().availableProcessors()));
		ProcessSummary result = ExecUtil.execUntil(env, TIMEOUT, x10.build());
		String output = result.stderr + "\n" + result.stdout;
		String message = x10.toString() + "\n";
		message += String.format(
				"No deadlock found stdout+stderr (exit value=%d):\n",
				result.exitValue);
		message += output;
		assertTrue(message, output.contains("Deadlock detected: ") || // detection
				output.contains("x10.lang.ClockUseException: Deadlock found")); // avoidance
	}

}
