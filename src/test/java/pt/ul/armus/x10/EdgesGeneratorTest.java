package pt.ul.armus.x10;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static pt.ul.armus.x10.EqualsToEdgeSet.edgeSet;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import pt.ul.armus.Resource;
import pt.ul.armus.deadlockresolver.DeadlockFoundException;
import pt.ul.armus.edgebuffer.TaskHandle;
import x10.lang.Clock;

public class EdgesGeneratorTest extends InitX10 {
	TaskHandle handle;
	EdgesGenerator gen;

	@Before
	public void setUpEdge() {
		handle = mock(TaskHandle.class);
		gen = new EdgesGenerator(activity, handle, Arrays.asList(activity.finishState));
	}

	@Test
	public void testBlockClock2Advance() throws DeadlockFoundException {
		Clock c1 = Clock.make();
		Clock c2 = Clock.make();
		gen.blockClockAdvance(c1);
		Resource c1_2 = new ClockVariable(c1, 2);
		Resource c1_1 = new ClockVariable(c1, 1);
		Resource c2_1 = new ClockVariable(c2, 1);
		Resource f = new FinishVariable(activity.finishState());
		verify(handle).setBlocked(argThat(edgeSet(containsInAnyOrder(c1_2, c2_1, f), containsInAnyOrder(c1_1))));
	}

	@Test
	public void testBlockClock1Advance() throws DeadlockFoundException {
		Clock c1 = Clock.make();
		gen.blockClockAdvance(c1);
		Resource c1_2 = new ClockVariable(c1, 2);
		Resource c1_1 = new ClockVariable(c1, 1);
		Resource f = new FinishVariable(activity.finishState());
		verify(handle).setBlocked(argThat(edgeSet(containsInAnyOrder(c1_2, f), containsInAnyOrder(c1_1))));
	}

	@Test
	public void testBlockClock1AdvanceAll() throws DeadlockFoundException {
		Clock c1 = Clock.make();
		gen.blockClockAdvanceAll();
		Resource c1_1 = new ClockVariable(c1, 1);
		Resource f = new FinishVariable(activity.finishState());
		verify(handle).setBlocked(argThat(edgeSet(containsInAnyOrder(f), containsInAnyOrder(c1_1))));
	}
	
}
