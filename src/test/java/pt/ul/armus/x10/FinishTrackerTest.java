package pt.ul.armus.x10;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import pt.ul.armus.x10.FinishTracker.FinishLink;
import pt.ul.armus.x10.FinishTracker.FinishLinkIterable;
import x10.lang.FinishState;
import x10.lang.Runtime;

public class FinishTrackerTest extends InitX10 {

	private <T> List<T> toList(Iterable<T> iterable) {
		ArrayList<T> lst = new ArrayList<>();
		for (T elem : iterable) {
			lst.add(elem);
		}
		return lst;
	}
	
	@Test
	public void elems1() {
		FinishState f1 = mock(FinishState.class);
		FinishLink link = new FinishLink(f1, null);
		FinishLinkIterable finishes = new FinishLinkIterable(link);
		Iterator<FinishState> iter = finishes.iterator();
		assertTrue(iter.hasNext());
		assertSame(f1, iter.next());
		assertFalse(iter.hasNext());
		assertEquals(asList(f1), toList(finishes));
	}
	
	@Test
	public void elems2() {
		FinishState f1 = mock(FinishState.class);
		FinishLink link1 = new FinishLink(f1, null);
		FinishState f2 = mock(FinishState.class);
		FinishLink link2 = new FinishLink(f2, link1);
		FinishLinkIterable finishes = new FinishLinkIterable(link2);
		assertEquals(asList(f2, f1), toList(finishes));
	}
	
	@Test
	public void testUnlinked() {
		FinishTracker tracker = new FinishTracker();
		FinishState f = mock(FinishState.class);
		Iterable<FinishState> finishTree = tracker.getFinishTree(f);
		assertEquals(asList(f), toList(finishTree));
	}

	@Test
	public void test1Linked() {
		FinishTracker tracker = new FinishTracker();
		FinishState f1 = mock(FinishState.class);
		tracker.linkToParent(f1, Runtime.activity().finishState);
		assertEquals(asList(f1, Runtime.activity().finishState), toList(tracker.getFinishTree(f1)));
	}

	@Test
	public void test2Linked() {
		FinishTracker tracker = new FinishTracker();
		FinishState f1 = mock(FinishState.class);
		FinishState f2 = mock(FinishState.class);
		FinishState f3 = mock(FinishState.class);
		tracker.linkToParent(f2, f1);
		tracker.linkToParent(f3, f2);
		assertEquals(asList(f2, f1), toList(tracker.getFinishTree(f2)));
	}

	@Test
	public void test3Linked() {
		FinishTracker tracker = new FinishTracker();
		FinishState f1 = mock(FinishState.class);
		FinishState f2 = mock(FinishState.class);
		FinishState f3 = mock(FinishState.class);
		tracker.linkToParent(f2, f1);
		tracker.linkToParent(f3, f2);
		assertEquals(asList(f3, f2, f1), toList(tracker.getFinishTree(f3)));
	}

}
