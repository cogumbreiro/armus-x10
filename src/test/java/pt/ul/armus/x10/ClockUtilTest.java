package pt.ul.armus.x10;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import x10.core.fun.VoidFun_0_0;
import x10.lang.Activity;
import x10.lang.Activity.ClockPhases;
import x10.lang.Clock;
import x10.lang.FinishState;
import x10.rtt.RuntimeType;
import x10.rtt.Type;
import x10.serialization.X10JavaSerializer;
import x10.x10rt.X10RT;
import static org.mockito.Mockito.*;

public class ClockUtilTest extends InitX10 {

	public ClockPhases phases;
	
	@Before
	@Override
	public void setUp() {
		super.setUp();
		phases = activity.clockPhases();
	}
	
	@Test
	public void testGetRegisteredClocks() {
		Clock clock = new Clock("foo");
		phases.put(clock, 999);
		assertEquals(phases.getOrThrow$O(clock), 999);
		assertEquals(clock.phase$O(), 999);
	}

	@Test
	public void testHasResumed() throws UnregisteredActivityException {
		Clock clock = new Clock("foo");
		phases.put(clock, -999);
		assertTrue(ClockUtil.hasResumed(activity, clock));
	}

	@Test
	public void testGetWaitPhase1() throws UnregisteredActivityException {
		Clock clock = new Clock("foo");
		phases.put(clock, -999);
		assertEquals(999, ClockUtil.getWaitPhase(activity, clock));
	}

	@Test
	public void testGetWaitPhase2() throws UnregisteredActivityException {
		Clock clock = new Clock("foo");
		phases.put(clock, 999);
		assertEquals(999, ClockUtil.getWaitPhase(activity, clock));
	}

	@Test
	public void testMake() throws UnregisteredActivityException {
		Clock clock = Clock.make();
		assertRegistered(clock);
		assertEquals(1, ClockUtil.getWaitPhase(activity, clock));
		assertEquals(1, ClockUtil.getSignalPhase(activity, clock));
	}
	
	@Test
	public void testResume() throws UnregisteredActivityException {
		Clock clock = Clock.make();
		clock.resume();
		assertEquals(1, ClockUtil.getWaitPhase(activity, clock));
		assertEquals(2, ClockUtil.getSignalPhase(activity, clock));
	}
	
	@Test
	public void testDrop() throws UnregisteredActivityException {
		Clock clock = Clock.make();
		clock.drop();
		assertDeregistered(clock);
	}

	@Test
	public void test2Clocks() throws UnregisteredActivityException {
		Clock clock1 = Clock.make();
		clock1.resume();
		Clock clock2 = Clock.make();
		assertEquals(1, ClockUtil.getWaitPhase(activity, clock1));
		assertEquals(2, ClockUtil.getSignalPhase(activity, clock1));
		assertEquals(1, ClockUtil.getWaitPhase(activity, clock2));
		assertEquals(1, ClockUtil.getSignalPhase(activity, clock2));
	}
	
	@Test
	public void testResumeAll1() throws UnregisteredActivityException {
		Clock clock1 = Clock.make();
		clock1.resume();
		Clock clock2 = Clock.make();
		Clock.resumeAll();
		assertEquals(1, ClockUtil.getWaitPhase(activity, clock1));
		assertEquals(2, ClockUtil.getSignalPhase(activity, clock1));
		assertEquals(1, ClockUtil.getWaitPhase(activity, clock2));
		assertEquals(2, ClockUtil.getSignalPhase(activity, clock2));
	}

	@Test
	public void testResumeAll2() throws UnregisteredActivityException {
		Clock clock1 = Clock.make();
		Clock clock2 = Clock.make();
		Clock.resumeAll();
		assertEquals(1, ClockUtil.getWaitPhase(activity, clock1));
		assertEquals(2, ClockUtil.getSignalPhase(activity, clock1));
		assertEquals(1, ClockUtil.getWaitPhase(activity, clock2));
		assertEquals(2, ClockUtil.getSignalPhase(activity, clock2));
	}


	@Test
	public void testAdvance() throws UnregisteredActivityException {
		Clock clock1 = Clock.make();
		clock1.advance();
		assertEquals(2, ClockUtil.getWaitPhase(activity, clock1));
		assertEquals(2, ClockUtil.getSignalPhase(activity, clock1));
	}

	@Test
	public void testAdvanceAll() throws UnregisteredActivityException {
		Clock clock1 = Clock.make();
		Clock.advanceAll();
		assertEquals(2, ClockUtil.getWaitPhase(activity, clock1));
		assertEquals(2, ClockUtil.getSignalPhase(activity, clock1));
	}
}
