package pt.ul.armus.x10;

import java.util.Collection;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import pt.ul.armus.Resource;
import pt.ul.armus.edgebuffer.EdgeSet;

class EqualsToEdgeSet extends TypeSafeMatcher<EdgeSet> {
	private Matcher<Iterable<? extends Resource>> pred;
	private Matcher<Iterable<? extends Resource>> succ;

	public EqualsToEdgeSet(Matcher<Iterable<? extends Resource>> pred, Matcher<Iterable<? extends Resource>> succ) {
		this.pred = pred;
		this.succ = succ;
	}

	@Override
	public void describeTo(Description desc) {
		desc.appendText("successors=");
		desc.appendDescriptionOf(succ);
		desc.appendText(", predecessors=");
		desc.appendDescriptionOf(pred);
	}

	@Override
	protected boolean matchesSafely(EdgeSet other) {
		return pred.matches(other.getAllocated())
				&& succ.matches(other.getRequested());
	}
	
	public static Matcher<EdgeSet> edgeSet(Matcher<Iterable<? extends Resource>> pred, Matcher<Iterable<? extends Resource>> succ) {
		return new EqualsToEdgeSet(pred, succ);
	}

}