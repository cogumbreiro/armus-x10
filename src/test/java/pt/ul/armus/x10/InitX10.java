package pt.ul.armus.x10;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.BeforeClass;

import x10.core.fun.VoidFun_0_0;
import x10.lang.Activity;
import x10.lang.Activity.ClockPhases;
import x10.lang.Clock;
import x10.lang.FinishState;
import x10.lang.Place;
import x10.x10rt.X10RT;

public class InitX10 {
	protected Activity activity;
	
	@BeforeClass
	public static void setUpClass() {
		X10RT.init();
	}

	/**
	 * Override to define your own activity.
	 * @return
	 */
	protected Activity createDefaultActivity() {
		return createActivity();
	}
	
	/**
	 * Create an activity.
	 * @return
	 */
	public static Activity createActivity() {
		return createActivity(x10.lang.Runtime.makeDefaultFinish());
	}

	/**
	 * Create an activity, but define its initial finish state.
	 * @return
	 */
	public static Activity createActivity(FinishState st) {
		VoidFun_0_0 body = mock(VoidFun_0_0.class);
		Place home = x10.lang.Runtime.home();
		return new Activity(body, home, st, new ClockPhases());
	}
	
	@Before
	public void setUp() {
		activity = createDefaultActivity();
		setCurrentActivity(activity);
	}
	
	/**
	 * Overrides the current activity.
	 * @param act
	 */
	public static void setCurrentActivity(Activity act) {
		x10.lang.Runtime.worker().activity = act;
		assertSame(act, x10.lang.Runtime.activity());
	}
	
	/**
	 * Registers the current activity with the given clock.
	 * @param clock
	 */
	public static void registerRawCurrentActivity(Clock clock, int phase) {
		registerRawWith(x10.lang.Runtime.activity(), clock, phase);
	}
	
	/**
	 * Registers the current activity with the given clock.
	 * @param clock
	 */
	public static void registerRawWith(Activity act, Clock clock, int phase) {
		act.clockPhases().put(clock, phase);
	}
	
	protected void assertRegistered(Clock clock) {
		assertTrue(x10.lang.Runtime.activity().clockPhases().containsKey__0x10$util$HashMap$$K$O(clock));
	}

	protected void assertDeregistered(Clock clock) {
		assertFalse(x10.lang.Runtime.activity().clockPhases().containsKey__0x10$util$HashMap$$K$O(clock));
	}
}
