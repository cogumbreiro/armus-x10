package pt.ul.armus.x10;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.util.Arrays;

import org.junit.Test;

import pt.ul.armus.Resource;
import x10.lang.Clock;

public class ClockManagerTest extends InitX10 {

	@Test
	public void testClockManager() {
		ClockManager man = new ClockManager(activity);
		assertTrue(man.getAllocated().isEmpty());
		assertTrue(man.requestAll().isEmpty());
	}

	@Test
	public void oneClockAllocated() {
		ClockManager man = new ClockManager(activity);
		Clock clock = Clock.make();
		Resource res = new ClockVariable(clock, 1);
		assertThat(man.getAllocated(), containsInAnyOrder(res));
	}

	@Test
	public void oneClockAllocated2() {
		ClockManager man = new ClockManager(activity);
		Clock clock = Clock.make();
		clock.resume();
		Resource res = new ClockVariable(clock, 2);
		assertThat(man.getAllocated(), containsInAnyOrder(res));
	}

	@Test
	public void oneClockRequestAll() {
		ClockManager man = new ClockManager(activity);
		Clock clock = Clock.make();
		Resource res = new ClockVariable(clock, 1);
		assertThat(man.requestAll(), containsInAnyOrder(res));
	}

	@Test
	public void oneClockRequestAll2() {
		ClockManager man = new ClockManager(activity);
		Clock clock = Clock.make();
		clock.resume();
		Resource res = new ClockVariable(clock, 1);
		assertThat(man.requestAll(), containsInAnyOrder(res));
	}

	@Test
	public void testRequestOne() throws UnregisteredActivityException {
		ClockManager man = new ClockManager(activity);
		Clock clock = Clock.make();
		Resource res = new ClockVariable(clock, 1);
		assertThat(man.requestOne(clock), equalTo(res));
	}

	@Test
	public void testRequestOne2() throws UnregisteredActivityException {
		ClockManager man = new ClockManager(activity);
		Clock clock = Clock.make();
		clock.resume();
		Resource res = new ClockVariable(clock, 1);
		assertThat(man.requestOne(clock), equalTo(res));
	}

	@Test
	public void testAdvanceAndGetAllocated() throws UnregisteredActivityException {
		ClockManager man = new ClockManager(activity);
		Clock clock = Clock.make();
		Resource res = new ClockVariable(clock, 2);
		assertThat(man.advanceAndGetAllocated(clock), containsInAnyOrder(res));
	}

	@Test
	public void testAdvanceAndGetAllocated2() throws UnregisteredActivityException {
		ClockManager man = new ClockManager(activity);
		Clock clock = Clock.make();
		clock.resume();
		Resource res = new ClockVariable(clock, 2);
		assertThat(man.advanceAndGetAllocated(clock), containsInAnyOrder(res));
	}
}
