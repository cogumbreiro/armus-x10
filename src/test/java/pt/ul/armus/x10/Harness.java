package pt.ul.armus.x10;

import static pt.ul.armus.x10.ExecUtil.joinPath;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import pt.ul.armus.conf.DetectionConfiguration;
import pt.ul.armus.conf.EdgeBufferConfiguration;
import pt.ul.armus.conf.GraphStrategy;
import pt.ul.armus.conf.MainConfiguration;
import pt.ul.armus.cycledetector.jgraph.JGraphTSolver;
import pt.ul.armus.deadlockresolver.DeadlockResolver;
import pt.ul.armus.edgebuffer.EdgeBufferFactory;
import pt.ul.armus.edgebuffer.HashEdgeBufferFactory;
import pt.ul.armus.edgebuffer.QueueEdgeBufferFactory;

public class Harness {
	private static final String POS_SAMPLES_DIR = joinPath("src", "pos-samples", "x10");
	private static final String NEG_SAMPLES_DIR = joinPath("src", "neg-samples", "x10");
	
	/**
	 * Returns the java builder with the default settings to use JArmus.
	 * @param className
	 * @return
	 */
	public static X10ProcessBuilder x10(String className) {
		X10ProcessBuilder x10 = new X10ProcessBuilder(className);
		x10.appendClasspath("target/pos-samples.jar");
		x10.appendClasspath("target/neg-samples.jar");
		return x10;
	}
	
	/**
	 * Get the class files in a directory
	 * @param directory
	 * @return
	 * @throws FileNotFoundException 
	 */
	private static Collection<String> getClasses(String directory) throws IOException {
		List<String> result = new ArrayList<>();
		String[] files = new File(directory).list();
		if (files == null) {
			throw new IOException("Cannot list directory: " + directory);
		}
		for (String filename : files) {
			String suffix = ".x10";
			if (filename.endsWith(suffix)) {
            	result.add(filename.substring(0, filename.length() - suffix.length()));
            }
	    }
	    Collections.sort(result);
	    return result;
	}
	
	public String getCurrentPackage() {
		String className = getClass().getName();
		int endIndex = className.lastIndexOf('.');
		if (endIndex == -1) {
			return null;
		} else {
			return className.substring(0, endIndex);
		}
		
	}
	
	private static String getPositiveTestsDir() {
		return POS_SAMPLES_DIR;
	}
	
	private static String getNegativeTestsDir() {
		return NEG_SAMPLES_DIR;
	}
	
	public static Collection<String> getPositiveTestNames() throws IOException {
		return getClasses(getPositiveTestsDir());
	}
	
	public static Collection<String> getNegativeTestNames() throws IOException {
		return getClasses(getNegativeTestsDir());
	}
	
	private static EdgeBufferConfiguration[] getBufferConf() {
		EdgeBufferFactory[] buffers = { new HashEdgeBufferFactory(),
				new QueueEdgeBufferFactory() };
		EdgeBufferConfiguration[] conf = new EdgeBufferConfiguration[buffers.length];
		int index = 0;
		for (EdgeBufferFactory fact : buffers) {
			conf[index] = new EdgeBufferConfiguration(true, fact);
			index++;
		}
		return conf;
	}

	public static Collection<MainConfiguration> getConfigurations() {
		ArrayList<MainConfiguration> result = new ArrayList<>();
		@SuppressWarnings("unused")
		boolean[] booleans = new boolean[] { true, false };
		for (GraphStrategy graph : GraphStrategy.values()) {
			//for (boolean avoidanceEnabled : booleans) {
			boolean avoidanceEnabled = false; // XXX: currently we cannot support avoidance
				boolean detectionEnabled = !avoidanceEnabled;
				for (EdgeBufferConfiguration eConf : getBufferConf()) {
					DetectionConfiguration dConf = new DetectionConfiguration(
							detectionEnabled, 0, 1);
					JGraphTSolver detector = new JGraphTSolver();
					DeadlockResolver resolver = X10DeadlockResolver.DEFAULT;
					MainConfiguration conf = new MainConfiguration(dConf,
							eConf, avoidanceEnabled, detector, resolver, graph, false);
					result.add(conf);
				}
			//}
		}
		return result;
	}
	
}
