package pt.ul.armus.x10;

import static org.junit.Assert.*;

import org.junit.Test;

import x10.lang.Clock;

public class ClockVariableTest extends InitX10 {

	@Test
	public void test() throws UnregisteredActivityException {
		Clock c = Clock.make();
		ClockVariable v1 = new ClockVariable(c, 10);
		ClockVariable v2 = new ClockVariable(c, 10);
		assertTrue(v1.equals(v2));
		assertEquals(v1.hashCode(), v2.hashCode());
	}

}
