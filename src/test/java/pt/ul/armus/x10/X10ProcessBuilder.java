package pt.ul.armus.x10;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

public class X10ProcessBuilder {
	private String x10;
	private Collection<String> classpath = new ArrayList<>();
	private String javaAgent;
	private String javaAgentArgs;
	private String program;
	private Collection<String> programArgs = new ArrayList<>();
	private Map<String, String> systemProps = new LinkedHashMap<>();
	public X10ProcessBuilder(String jvm, String program) {
		this.x10 = jvm;
		this.program = program;
	}
	public X10ProcessBuilder(String program) {
		this("x10", program);
	}
	public String getJvm() {
		return x10;
	}
	public void setJvm(String jvm) {
		this.x10 = jvm;
	}
	public void setProgram(String program) {
		this.program = program;
	}
	public String getProgram() {
		return program;
	}
	public void appendClasspath(String param) {
		classpath.add(param);
	}
	public void appendProgramArgument(String arg) {
		programArgs.add(arg);
	}
	public void setJavaAgent(String param) {
		this.javaAgent = param;
	}
	public String getJavaAgent() {
		return javaAgent;
	}
	public void setJavaAgentArgs(String javaAgentArgs) {
		this.javaAgentArgs = javaAgentArgs;
	}
	public String getJavaAgentArgs() {
		return javaAgentArgs;
	}
	
	public void setSystemProperty(String key) {
		setSystemProperty(key, null);
	}
	
	public void setSystemProperty(String key, String value) {
		systemProps.put(key, value);
	}
	
	public void unsetSystemProperty(String key) {
		systemProps.remove(key);
	}
	
	private String buildClasspath() {
		return ExecUtil.join(File.pathSeparator, classpath);
	}
	
	private String buildJavaAgent() {
		StringBuilder builder = new StringBuilder("-J-javaagent:");
		builder.append(javaAgent);
		if (javaAgentArgs != null) {
			builder.append('=');
			builder.append(javaAgentArgs);
		}
		return builder.toString();
	}
	
	public String[] build() {
		Collection<String> result = new ArrayList<>();
		result.add(x10);
		if (classpath.size() > 0) {
			result.add("-cp");
			result.add(buildClasspath());
		}
		if (javaAgent != null) {
			result.add(buildJavaAgent());
		}
		buildSystemProperties(result);
		result.add(program);
		result.addAll(programArgs);
		return result.toArray(new String[0]);
	}
	private void buildSystemProperties(Collection<String> result) {
		for (Entry<String, String> entry : systemProps.entrySet()) {
			StringBuilder builder = new StringBuilder();
			builder.append("-D");
			builder.append(entry.getKey());
			if (entry.getValue() != null) {
				builder.append("=");
				builder.append(entry.getValue());
			}
			result.add(builder.toString());
		}
	}
	@Override
	public String toString() {
		return ExecUtil.join(" ", build());
	}
	public void setSystemProperties(Properties props) {
		systemProps.clear();
		for (String key : props.stringPropertyNames()) {
			systemProps.put(key, props.getProperty(key));
		}
	}
}