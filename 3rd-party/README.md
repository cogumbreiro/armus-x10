Warning
-------

The publishing procedure is tricky because since `soot` depends on `polyglot`
and on `jasmin` it will break upon populating the repository for the first
time.

So, if you have a new repository, do the following:

 1. `make`
 2. `ant` (this will break)
 3. `cd ../target/3rd-party`
 4. `git add *`
 5. `git commit -a -m "added files"`
 6. `git push"`
 7. `cd ../../3rd-party`
 8. `ant` (should work now, just push your changes) 

Sorry, but I don't have time to fix it. Patches are welcome.
 